﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TodoApp.DAL;
using TodoApp.Controllers;
using System.Web.Mvc;

namespace TodoApp.Tests
{
    /// <summary>
    /// Description résumée pour TodoControllerTest
    /// </summary>
    [TestClass]
    public class TodoControllerTest
    {
        private readonly TodoController controller = new TodoController();
        private readonly TodoContext context = new TodoContext();

        [TestMethod]
        public void Todoes()
        {
            var result = controller.Index() as ViewResult;
            Assert.IsNotNull(result);
        }
    }
}
